<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZA@^wDkKzEBfkXA(h|`@2:iuVA6ijB8MjT9i0M7<A/zN!]Q(1UMzQ&era_:M|e8c');
define('SECURE_AUTH_KEY',  ')6{DhZE`HWi4FAz4Q3jju=11A-65_X; PLPb1=Jm-77jw}a/YD1Z5!zbCsf 1=Ua');
define('LOGGED_IN_KEY',    'A6J6Cv>f4<OX+j*9<>z}8;xGg[3E(oUa4t;*~TGW[HQIk:gCV@TOi.@?ON}7`E7N');
define('NONCE_KEY',        'w<qWLYiWgBFXlXv@Dh2@wBUf8LK!tZSK#A_0s?*T%+8 #!<wO`CdA$h/QHxQPem4');
define('AUTH_SALT',        '75NzQrhpv ?nNyXV~a#[XE.dmSL@W^rX}|d_d5]e>vqgrg^J%tm46]8KhdS{IC,K');
define('SECURE_AUTH_SALT', '4G@]cZs=[B.iEWN(jCMz9Q8us27.-yF*4_&H^CC53x1lRUF^j{5i<exZ3wN-54A~');
define('LOGGED_IN_SALT',   '*E.n87QRJnC(<pc]G7<,r%g 4k1]CSVq6J<;6qYR4[luYcXA[.#,-$i*6YB >ptX');
define('NONCE_SALT',       '^juQ|~wb-U,I(}bNwgbT=}7?-/W,#m<((Jw+7OAhBq[K&o]li1C~K7]-[P@>5|l_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
