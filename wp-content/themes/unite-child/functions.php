<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

/**
@Desc Function to import parent theme style css into child theme unite
*/

function enqueue_parent_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

/*
* Creating a function to create our CPT
*/

/**
@Desc Function to create custom post type for films
*/
function custom_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Films', 'Post Type General Name', 'unite-child' ),
		'singular_name'       => _x( 'Film', 'Post Type Singular Name', 'unite-child' ),
		'menu_name'           => __( 'Films', 'unite-child' ),
		'parent_item_colon'   => __( 'Parent Film', 'unite-child' ),
		'all_items'           => __( 'All Films', 'unite-child' ),
		'view_item'           => __( 'View Film', 'unite-child' ),
		'add_new_item'        => __( 'Add New Film', 'unite-child' ),
		'add_new'             => __( 'Add New', 'unite-child' ),
		'edit_item'           => __( 'Edit Film', 'unite-child' ),
		'update_item'         => __( 'Update Film', 'unite-child' ),
		'search_items'        => __( 'Search Film', 'unite-child' ),
		'not_found'           => __( 'Not Found', 'unite-child' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'unite-child' ),
	);

	// Set other options for Custom Post Type Film

	$args = array(
		'label'               => __( 'films', 'unite-child' ),
		'description'         => __( 'New Films', 'unite-child' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
	register_post_type( 'films', $args );
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'custom_post_type', 0 );

/**
@desc Function to create taxonomies
*/

function themes_taxonomy() {  
	register_taxonomy(  
        'films_categories', 
        'films',
        array(  
        	'hierarchical' => true,  
            'label' => 'Categories',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'films_categories',
                'with_front' => false // Don't display the category base before 
            )
        )  
    );  
}  
add_action( 'init', 'themes_taxonomy');

/**
@Desc Function to create short code to display last 5 posts...
*/

function test_films_fun($attr) {
	$html = '';
	wp_reset_query();
	$limit = $atts['limit'];

	if(!isset($limit)) {
		$limit = 5;
	}

	$loop = new WP_Query(array('post_type' => 'films','posts_per_page' => $limit));
	while ( $loop->have_posts() ) {
		$html .= '<div>';
		$loop->the_post();
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
		$html .= '<h3>'. get_the_title() .'</h3>';
		$html .= '<p><label>Ticket Price: </label>' . get_post_meta(get_the_ID(), 'ticket_price',true) .'</p>';
		$html .= '<p><label>Release Date: </label>' . get_post_meta(get_the_ID(), 'release_date',true) .'</p>';
		$html .= '</div>';
	}
	return $html;
}

add_shortcode( 'test_films', 'test_films_fun' );