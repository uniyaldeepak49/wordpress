<?php
/**
 * The Template for displaying all single posts.
 *
 * @package unite
 */
	get_header(); ?>
	<div id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
		<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
			<p><label>Ticket Price: </label><?php echo get_post_meta(get_the_id(), 'ticket_price',true) ?></p>

			<p><label>Release Date: </label> <?php echo get_post_meta(get_the_id(), 'release_date',true) ?></p>

			<p><label>Categories:</label>
				<?php $terms = wp_get_post_terms($post->ID, 'films_categories');
				if ($terms) {
				    $out = array();
				    foreach ($terms as $term) {
				        $out[] = '<a class="' .$term->slug .'" href="' . get_term_link( $term->slug, 'films_categories') .'">' .$term->name .'</a>';
				    }
				    echo join( ', ', $out );
				} ?>
			</p>

			<?php /*unite_post_nav();*/ ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>