<?php
get_header();
global $wp_query;
if ( get_query_var('paged') ) {
	$paged = get_query_var('paged');
} elseif ( get_query_var('page') ) {
	$paged = get_query_var('page');
} else {
	$paged = 1;
}
$posts_per_page = 10;
$loop = new WP_Query(  array( 'post_type' => 'films', 'posts_per_page' => $posts_per_page,'paged' => $paged ) );  

?>
<section class="pulse_blog">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<!--- blog start -->
				<?php 
				if ($loop->have_posts()) { ?>
					<?php 
					$i=0;	
					while ($loop->have_posts()) : $loop->the_post(); 
						$i=$i+1;
						?>
						<div class="border-b">
							<div class="row">
								<div class="col-sm-5">
									<div class="blog_img">
										<?php if ( has_post_thumbnail()) : 
											$get_the_post_thumbnail_url=get_the_post_thumbnail_url();
											?>
											<img src="<?php echo $get_the_post_thumbnail_url; ?>" alt="" class="img-fluid">
										<?php endif; ?>
									</div>
								</div>
								<div class="col-sm-7 d-flex align-items-center">
									<div class="blog_content">
										<h5><?php the_title(); ?></h5>
										<ul>
											<li><span><i class="fa fa-user" aria-hidden="true"></i></span>By <?php the_author_meta( 'display_name' ); ?></li>
											<li><span><i class="fa fa-calendar" aria-hidden="true"></i></span><?php echo get_the_date(); ?></li>
										</ul>
										<p><?php echo strip_tags(get_the_excerpt()); ?></p>
										<a href="<?php the_permalink(); ?>">Read More</a>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php } else{
					echo "Sorry, no posts matched your criteria."; 
				}
				?>
				<!--- blog end -->	
			</div>
			
			<!--- Sidebar start -->	
			<?php get_sidebar(); ?>
			<!--- Sidebar end -->	
		</div>

		<div class="row">
			<div class="col-sm-12">
				<?php
				wp_reset_query();
				if ( $loop->have_posts() ) :
					$GLOBALS['wp_query'] = $loop; 		
					$base=get_page_link().'?paged=%#%';
					$pagination=get_the_posts_pagination( array( 

						'mid_size' => 2,
						'prev_text' => __( '<span class="page-link" aria-hidden="true">&laquo;</span>
							<span class="sr-only">Back</span>', 'textdomain' ),
						'next_text' => __( '<span class="page-link" aria-hidden="true">&raquo;</span>
							<span class="sr-only">Next</span>', 'textdomain' ),
						'base' => $base,
						'title'              => '', 
						'type'        => 'list',
						'before_page_number' => '<span class="page-link">',
						'after_page_number' => '</span>',

					) );

					echo $pagination;
				else :
				endif;
				wp_reset_query();    								
				?>
			</div>
		</div>

		
	</div>
</section>
<?php get_footer(); ?>